Some algorithmic code snippets that I used in the past few years, mainly for programming contests. Templates are categorized as follows:

- basic algorithms
- data structures
- dynamic programming
- search algorithms
- math related algorithms
- computational geometry
- graph theory algorithms
- string related algorithms
- miscellaneous in the contest

Since I am no longer participating in algorithm competitions, the code in this repository will only be updated occasionally. If you have any question about the usage of template codes, welcome to submit pull request :)
