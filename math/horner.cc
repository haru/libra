//https://zh.wikipedia.org/wiki/%E7%A7%A6%E4%B9%9D%E9%9F%B6%E7%AE%97%E6%B3%95
//例题：https://www.acwing.com/activity/content/problem/content/8270/
//秦九韶算法
//有的题注意要写高精度，不如直接用python

#include <bits/stdc++.h>
using namespace std;
using ll = long long;

ll get() {
  int n, b;
  cin >> n >> b;
  ll res = 0;
  while(n -- ) {
    int x;
    cin >> x;
    res = res * b + x;
  }
  return res;
}

void run_case() {
  ll x = get();
  ll y = get();
  if(x < y) puts("<");
  else if(x == y) puts("=");
  else puts(">");
}

signed main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  int tests = 1;
  // cin >> tests;
  while (tests-- > 0)
    run_case();
  return 0;
}

